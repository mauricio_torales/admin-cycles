<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TallesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TallesTable Test Case
 */
class TallesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TallesTable
     */
    public $Talles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Talles',
        'app.Productos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Talles') ? [] : ['className' => TallesTable::class];
        $this->Talles = TableRegistry::getTableLocator()->get('Talles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Talles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
