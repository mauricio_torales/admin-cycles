<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ColoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ColoresTable Test Case
 */
class ColoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ColoresTable
     */
    public $Colores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Colores',
        'app.Productos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Colores') ? [] : ['className' => ColoresTable::class];
        $this->Colores = TableRegistry::getTableLocator()->get('Colores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Colores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
