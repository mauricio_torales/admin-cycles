<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CYCLES :';
?>

<!DOCTYPE html>
<html class="fixed sidebar-left-collapsed" lang="es">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('vendor/bootstrap/css/bootstrap.css') ?>
    <?= $this->Html->css('vendor/font-awesome/css/font-awesome.css') ?>
    <?= $this->Html->css('vendor/pnotify/pnotify.custom.css') ?>
     <?= $this->Html->css('vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') ?>

    <?= $this->Html->css('vendor/select2/select2.css') ?>
    <?= $this->Html->css('vendor/datatables/assets/css/datatables.css') ?>


    <?= $this->Html->css('theme.css') ?>
    <?= $this->Html->css('file.css') ?>
    <?= $this->Html->css('theme-custom.css') ?>
    <?= $this->Html->css('theme-admin-extension.css') ?>

     <?= $this->Html->script('vendor/modernizr/modernizr.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<section class="body">
    <?= $this->element('menu') ?>
    <section role="main" class="content-body">

    <?= $this->Flash->render() ?> 

  
    <?= $this->fetch('content') ?>
    
    </section>
</div>
</section>
    
<footer>

</footer>

<?= $this->Html->script('vendor/jquery/jquery.js') ?>
<?= $this->Html->script('vendor/jquery-browser-mobile/jquery.browser.mobile.js') ?>
<?= $this->Html->script('vendor/bootstrap/js/bootstrap.js') ?>
<?= $this->Html->script('vendor/nanoscroller/nanoscroller.js') ?>
<?= $this->Html->script('vendor/bootstrap-colorpicker/js/bootstrap-colorpicker') ?>
<?= $this->Html->script('vendor/jquery-validation/jquery.validate.js') ?>
<?= $this->Html->script('vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') ?>
<?= $this->Html->script('vendor/pnotify/pnotify.custom.js') ?>

<?= $this->Html->script('vendor/select2/select2.js') ?>
<?= $this->Html->script('vendor/jquery.dataTables.js') ?>
<?= $this->Html->script('vendor/datatables.js') ?>
<?= $this->Html->script('vendor/tabla.js') ?>






<?= $this->Html->script('theme.js') ?>
<?= $this->Html->script('theme.custom.js') ?>
<?= $this->Html->script('theme.init.js') ?>
<?= $this->Html->script('forms/examples.wizard.js') ?>



</body>
</html>
