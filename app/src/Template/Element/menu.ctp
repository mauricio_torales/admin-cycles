<section class="body"> 

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<?php echo $this->Html->image('logo-negro.png', ['alt' => 'Logo', 'height'=> '35']); ?>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			<img src="" height="">
			
			</header>
			<!-- end: header -->
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li>
										<a href="javascript: void(0);">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Inicio</span>
										</a>
									</li>
									
									<li class="nav-parent">
										<a>
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
											<span>Productos</span>
										</a>
										<ul class="nav nav-children">
											<li>
											<?php echo $this->Html->link('Listar productos', [
										          'controller' => 'Productos',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar producto', [
										          'controller' => 'Productos',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-bookmark-o" aria-hidden="true"></i>
											<span>Marcas</span>
										</a>
										<ul class="nav nav-children">
											<li>
											<?php echo $this->Html->link('Listar marcas', [
										          'controller' => 'Marcas',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar marca', [
										          'controller' => 'Marcas',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-sitemap" aria-hidden="true"></i>
											<span>Categorias</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar categorias', [
										          'controller' => 'Categorias',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar categoria', [
										          'controller' => 'Categorias',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tint" aria-hidden="true"></i>
											<span>Colores</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar colores', [
										          'controller' => 'Colores',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar color', [
										          'controller' => 'Colores',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tag" aria-hidden="true"></i>
											<span>Talles</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar tamaños', [
										          'controller' => 'Talles',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar tamaño', [
										          'controller' => 'Talles',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li >
									<?php $id=$this->Session->read('Auth.User.id');
									echo'
										<a href="http://www.cyclesport.com.py/admin-cycles/app/users/edit/'.$id.'">
											<i class="fa fa-tag" aria-hidden="true"></i>
											<span>Cambiar contraseña</span>
										</a>';
									?>

									</li>
									<li>
										<?php echo $this->Html->link('Salir', [
										          'controller' => 'Users',
										          'action' => 'logout',
										      ])
										      ; ?>
									</li>
									
									
								</ul>
							</nav>
							
						</div>
				
					</div>
				
				</aside>
				