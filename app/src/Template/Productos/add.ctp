<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Producto $producto
 */
?>
<header class="page-header">
    <h2>Agregar producto</h2>   
</header>


<div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title"></h2>
                                    </header>
                                    <div class="panel-body">


    <?= $this->Form->create($producto, ['type' => 'file']) ?>
  
        <?php
            
            
            echo $this->Form->control('nombre', [
            'templates' => [
                'inputContainer' => '<div class="form-group">
                                                        <label class="col-sm-3 control-label" >Nombre</label>
                                                        <div class="col-sm-6">{{content}}</div></div>'
            ],
            "class" => "form-control",
            "required",
            'label' => false
            ]);
           echo $this->Form->control('descripcion', [
            'templates' => [
                'inputContainer' => '<div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-username">Descripcion</label>
                                                        <div class="col-sm-6">{{content}}</div></div>'
            ],
            "class" => "form-control",
            "required"=> false,
            'label' => false
            ]);
           echo $this->Form->control('codigo', [
            'templates' => [
                'inputContainer' => '<div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-username">Codigo</label>
                                                        <div class="col-sm-6">{{content}}</div></div>'
            ],
            "class" => "form-control",
            "required",
            'label' => false
            ]);?>
            <div class="form-group">
                                                        <label class="col-sm-3 control-label" > Moneda</label>
                                                        <div class="col-md-6">
                                                           
                                                                <select name="moneda" id="moneda" >
                                                                <option value="0">Dolar</option>
                                                                <option value="1">Guaranies</option>
                                                                </select>
                                                               
                                                        
                                                        </div>
                                                    </div>
           <?php
           echo $this->Form->control('precio', [
            'templates' => [
                'inputContainer' => '<div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-username">Precio</label>
                                                        <div class="col-sm-6">{{content}}</div></div>'
            ],
            "class" => "form-control",
            "required"=> false,
            'label' => false
            ]);
            ?>
                                                     <div class="form-group">
                                                        <label class="col-sm-3 control-label" >Destacar producto</label>
                                                        <div class="col-md-6">
                                                           
                                                                <select name="destacado" id="destacado" >
                                                                <option value="0">No</option>
                                                                <option value="1">Si</option>
                                                                </select>
                                                               
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputSuccess">Marca</label>
                                                <div class="col-md-6">
                                                   
                                                 <?php  echo $this->Form->control('marca_id',['class' => 'form-control mb-md','label' => false], ['options' => $marcas]);
                                                         ?>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputSuccess">Categoria</label>
                                                <div class="col-md-6">
                                                   
                                                 <?php
                                                        echo $this->Form->control('categoria_id',['class' => 'form-control mb-md','label' => false], ['options' => $categorias]); ?>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputSuccess">Colores</label>
                                                <div class="col-md-6" style="text-transform: uppercase;">
                                                   
                                                 <?php
                                                        
                                                        echo $this->Form->control('colores._ids',['data-plugin-selectTwo','class' => 'form-control populate','style'=>'text-transform:uppercase;','label' => false],['options' => $colores]);

                                                         ?>
                                                    
                                                </div>
                                            </div>
                                               
                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputSuccess">Tamaños</label>
                                                <div class="col-md-6">
                                                   
                                                 <?php
                                                        
                                                        echo $this->Form->control('talles._ids',['data-plugin-selectTwo','class' => 'form-control populate','label' => false], ['options' => $talles]);

                                                         ?>
                                                    
                                                </div>
                                            </div>
                                            <div id="imagenes" class="wrapper">
                                                

                                              <div class="box">
                                                <div class="js--image-preview"></div>
                                                <div class="upload-options">
                                                  <label>
                                                    <input type="file" name="foto1" id="foto1" class="image-upload" accept="image/*" required/>
                                                  </label>
                                                </div>
                                              </div>

                                              <a id="btn1" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa  fa-file-image-o"></i> agregar otra imagen</a>

                                            </div>

                                           

    
                                            <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>

    <?= $this->Form->end() ?>


</div>


<script>
$(document).ready(function(){

  $("#btn1").click(function(){
    $("#imagenes").append(' <div class="box"><div class="js--image-preview"></div><div class="upload-options"><label><input type="file" name="foto2" id="foto2" class="image-upload" accept="image/*" /></label></div></div><a id="btn2" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa  fa-file-image-o"></i> agregar otra imagen</a></div>');
    $("#btn1").remove();
    function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for (let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}
  });

  $(document).on('click', '#btn2', function(e) {
    $("#imagenes").append(' <div class="box"><div class="js--image-preview"></div><div class="upload-options"><label><input type="file" name="foto3" id="foto3" class="image-upload" accept="image/*" /></label></div></div><a id="btn3" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa  fa-file-image-o"></i> agregar otra imagen</a></div>');
    $("#btn2").remove();
    function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for (let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}

    });
   $(document).on('click', '#btn3', function(e) {
    $("#imagenes").append(' <div class="box"><div class="js--image-preview"></div><div class="upload-options"><label><input type="file" name="foto4" id="foto4" class="image-upload" accept="image/*" /></label></div></div><a id="btn4" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa  fa-file-image-o"></i> agregar otra imagen</a></div>');
    $("#btn3").remove();
    function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for (let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}

    });
    $(document).on('click', '#btn4', function(e) {
    $("#imagenes").append(' <div class="box"><div class="js--image-preview"></div><div class="upload-options"><label><input type="file" name="foto5" id="foto5" class="image-upload" accept="image/*" /></label></div></div></div>');
    $("#btn4").remove();
    function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for (let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}

    });
  

  

});
</script>


<script >
function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for (let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}

</script>