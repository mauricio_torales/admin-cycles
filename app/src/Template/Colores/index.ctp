<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Colore[]|\Cake\Collection\CollectionInterface $colores
 */
?>

<header class="page-header">
    <h2>Colores</h2> 
</header>
 <div class="col-md-10">
        <section class="panel">
             <header class="panel-heading">
                   <div class="panel-actions">
                       <a href="#" class="fa fa-caret-down"></a>
                    </div>
                        <h2 class="panel-title">Lista de colores</h2>
                             </header>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table mb-none">
                                                <thead>
                                                    <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Color') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Codigo color') ?></th>
                <th scope="col" class="actions"><?= __('Eliminar') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($colores as $color): ?>
            <tr>
                <td><?= $this->Number->format($color->id) ?></td>
                <td><?= h($color->color) ?></td>
                <td><?= h($color->codigo_color) ?></td>
                <td class="actions">
                  <?= $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $color->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este producto?', $color->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
          <?= $this->Paginator->first('<< ' . __('inicio')) ?>
          <?= $this->Paginator->prev('< ' . __('anterior')) ?>
           <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('siguiente') . ' >') ?>
         <?= $this->Paginator->last(__('ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registros de {{count}} en total')]) ?></p>
    </div>
</div>
