<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Colore $colore
 */
?>
<header class="page-header">
    <h2>Agregar color</h2>   
</header>

  
                    <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title">Color Pickers</h2>
                                    </header>
                                    <div class="panel-body">
                        
                                        <?= $this->Form->create($colore) ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Nombre del color</label>
                                                <div class="col-md-6">

                                                    <input type="text" class="form-control" name="color" id="color">

                                                </div>
                                            </div>
                                            <!--
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Default</label>
                                                <div class="col-md-6">
                                                    <input type="text" data-plugin-colorpicker class="colorpicker-default form-control" value="#8fff00"/>
                                                </div>
                                            </div> -->
                                           
                                           
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Codigo color</label>
                                                <div class="col-md-6">
                                                    <div class="input-group color" data-color="rgb(255, 146, 180)" data-color-format="rgb" data-plugin-colorpicker>
                                                        <span class="input-group-addon"><i></i></span>

                                                        <input type="text" class="form-control" name="codigo_color" id="codigo_color">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
                                         <?= $this->Form->end() ?>
                                    </div>
                                </section>
                            </div>
                        </div>
                        
