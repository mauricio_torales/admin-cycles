<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Producto Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $marca_id
 * @property int $categoria_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $foto1_dir
 * @property string|null $foto1
 * @property string|null $foto2_dir
 * @property string|null $foto2
 * @property string|null $foto3_dir
 * @property string|null $foto3
 * @property string|null $foto4_dir
 * @property string|null $foto4
 * @property string|null $foto5_dir
 * @property string|null $foto5
 * @property string|null $destacado
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Marca $marca
 * @property \App\Model\Entity\Categoria $categoria
 * @property \App\Model\Entity\Colore[] $colores
 * @property \App\Model\Entity\Talle[] $talles
 */
class Producto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'marca_id' => true,
        'categoria_id' => true,
        'nombre' => true,
        'descripcion' => true,
        'foto1_dir' => true,
        'foto1' => true,
        'foto2_dir' => true,
        'foto2' => true,
        'foto3_dir' => true,
        'foto3' => true,
        'foto4_dir' => true,
        'foto4' => true,
        'foto5_dir' => true,
        'foto5' => true,
        'destacado' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'marca' => true,
        'categoria' => true,
        'colores' => true,
        'codigo' => true,
        'moneda' => true,
        'precio' => true,
        'talles' => true
    ];
}
